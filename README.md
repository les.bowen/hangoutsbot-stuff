*hangoutsbot.service*

To install on Ubuntu 15.04+ (and other systems with systemd), modify and copy to /etc/systemd/system/

If you run in a python venv, one approach is here: https://stackoverflow.com/questions/37211115/how-to-enable-a-virtualenv-in-a-systemd-service-unit